<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Registration Request</div>
        <div>
            <table style='border-collapse: collapse; border-spacing: 0;'>
                <tr>
                    <td colspan='2'>New Agent Registration</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td>{{ $name  }}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>{{ $email  }}</td>
                </tr>
                <tr>
                    <td>Postcode</td>
                    <td>:</td>
                    <td>{{ $postcode  }}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>:</td>
                    <td>{{ $address  }}</td>
                </tr>
                <tr>
                    <td>Contact</td>
                    <td>:</td>
                    <td>{{ $contact  }}</td>
                </tr>
                <tr>
                    <td>Smartphone</td>
                    <td>:</td>
                    <td>{{ $smartphone  }}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>
