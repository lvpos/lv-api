<!DOCTYPE html>
<html>
<head>
</head>
<body>
<p>Dear Customer,</p>

<p>
</p>

<p>
    <style>
        .rtitle {
            background: #ff7400;
            padding: 8px 10px;
            color: #fff;
            font-size: 16px;
            font-weight: bold;
            text-transform: uppercase;
        }

        #boxprint {
            background: #002d65;
            padding: 10px;
        }

        #boxprint table {
            width: 100%;
            border: 1px solid #e9e9e9;
            font-weight: normal;
            border-collapse: collapse;
            border-spacing: 0;
        }

        #boxprint table tr th {
            background: #e9e9e9;
            padding: 10px;
        }

        #boxprint table tr td {
            color: #fff;
            padding: 10px;
            border: 1px solid #e9e9e9;
            line-height: 30px;
        }

        #boxprint table a {
            color: #ff7400;
            font-weight: bold;
            text-decoration: none;
        }

        #boxprint table .left {
            text-align: left;
        }

        #boxprint table .capital {
            text-transform: capitalize;
        }

        #boxprint #shownav {
            background: #dcdbdb;
            margin-top: 0;
            padding: 10px 0;
            border-radius: 0 0 4px 4px;
            -moz-border-radius: 0 0 4px 4px;
            -webkit-border-radius: 0 0 4px 4px;
        }
    </style>

<div class='rtitle' style='padding: 0;'>
    <span style='display: inline-block; padding: 8px 0 8px 10px; border-radius: 4px 0 0 0;'>Order No &rsaquo; {!! $data['order']->order_no !!}</span>

    <div style='clear: both;'></div>
</div>
<div id='boxprint'>
    <table>
        <tr>
            <th style='width: 50%;'>Origin Address</th>
            <th style='width: 50%;'>Destination Address</th>
        </tr>
        <tr>
            <td style='vertical-align: top;'>
                {!! $data['customer']->client_name !!}<br>
                <?php $flg = 0;
                $address = (array)$data['order']->order_ori_address;
                foreach ($address as $addr):
                    if ($flg == 1) {
                        echo '#' . $addr;
                    } elseif ($flg == 2) {
                        echo '-' . $addr . '<br>';
                    } else {
                        echo $addr . '<br>';
                    }
                    $flg++;
                    ?>
                <?php
                endforeach;
                ?>
                Postcode : {!! $data['order']->order_ori_postcode !!}<br>
                Region : {!! $data['order']->order_ori_region !!}
            </td>
            <td style='vertical-align: top;'>
                {!! $data['order']->order_dest_name !!}<br>
                <?php $flg = 0;
                $address = (array)$data['order']->order_dest_address;
                foreach ($address as $addr):
                    if ($flg == 1) {
                        echo '#' . $addr;
                    } elseif ($flg == 2) {
                        echo '-' . $addr . '<br>';
                    } else {
                        echo $addr . '<br>';
                    }
                    $flg++;
                    ?>
                <?php
                endforeach;
                ?>
                Postcode : {!! $data['order']->order_dest_postcode !!}<br>
                Region : {!! $data['order']->order_dest_region !!}
            </td>
        </tr>
    </table>
    <table>
        <tr style='text-align: center;'>
            <th style='width: 25%;'>Order No</th>
            <th style='width: 25%;'>Date</th>
            <th style='width: 25%;'>Status</th>
            <th style='width: 25%;'>Note</th>
        </tr>
        <tr style='text-align: center; text-transform: capitalize;'>
            <td>{!! $data['order']->order_no !!}</td>
            <td>{!! date('Y-m-d H:i:s') !!}</td>
            <td>{!! $data['order']->order_status !!}</td>
            <td>{!! $data['order']->order_notes !!}</td>
        </tr>
    </table>
</div>
&nbsp;</p>
<p>(sent automatically by Parcel Express system)</p>
</body>
</html>

