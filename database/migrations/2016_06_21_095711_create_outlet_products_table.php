<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletProductsTable extends Migration
{

    public function up()
    {
        Schema::create('outletProducts', function (Blueprint $table) {
            $table->increments('outlet_product_id');
            $table->integer('outlet_id');
            $table->integer('product_id');
            $table->unique(['outlet_id', 'product_id'], 'outlet_id_product_id_');
        });

        DB::table('outletProducts')->insert(
            [
                'outlet_id' => 1,
                'product_id' => 1
            ]
        );

    }

    public function down()
    {
        Schema::drop('outletProducts');
    }
}
