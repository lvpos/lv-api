<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateMenusTable extends Migration
{

    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->string('menu_name', 50);
            $table->string('menu_url')->nullable();
            $table->integer('menu_parent_id')->default(0);
            $table->tinyInteger('menu_priority');
            $table->boolean('is_active')->default(true);
            $table->boolean('is_parent')->default(false);
            $table->boolean('system_created')->default(false);
            $table->index('menu_parent_id', 'menu_parent_id');
            $table->index('is_active', 'is_active');
            $table->index('is_parent', 'is_parent');
            $table->index('system_created', 'system_created');
        });

        DB::table('menus')->insert(
            ['menu_name' => 'Home'
                , 'menu_url' => 'home'
                , 'menu_parent_id' => 0
                , 'menu_priority' => 1
                , 'is_parent' => true
                , 'system_created' => true
            ]);
        DB::table('menus')->insert(
            ['menu_name' => 'Menus'
                , 'menu_url' => 'menus'
                , 'menu_parent_id' => 0
                , 'menu_priority' => 2
                , 'is_parent' => true
                , 'system_created' => true
            ]);
        DB::table('menus')->insert(
            ['menu_name' => 'Roles'
                , 'menu_url' => 'roles'
                , 'menu_parent_id' => 0
                , 'menu_priority' => 3
                , 'system_created' => true
            ]);
        DB::table('menus')->insert(
            ['menu_name' => 'Users'
                , 'menu_url' => 'users'
                , 'menu_parent_id' => 0
                , 'menu_priority' => 4
                , 'is_parent' => true
                , 'system_created' => true
            ]);
        DB::table('menus')->insert(
            ['menu_name' => 'Products'
                , 'menu_url' => 'products'
                , 'menu_parent_id' => 0
                , 'menu_priority' => 5
                , 'is_parent' => true
                , 'system_created' => true
            ]);
        DB::table('menus')->insert(
            ['menu_name' => 'Outlets'
                , 'menu_url' => 'outlets'
                , 'menu_parent_id' => 0
                , 'menu_priority' => 6
                , 'is_parent' => true
                , 'system_created' => true
            ]);

    }

    public function down()
    {
        Schema::drop('menus');
    }
}
