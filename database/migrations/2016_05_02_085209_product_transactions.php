<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductTransactions extends Migration
{

    public function up()
    {
        Schema::create('productTrans', function (Blueprint $table) {
            $table->increments('trans_id');
            $table->date('trans_date');
            $table->string('trans_no');
            $table->text('trans_desc');
            $table->enum('trans_type', ['ADJ', 'SAL', 'BGN', 'RCV', 'UDF'])->default('BGN');
            $table->integer('product_id');
            $table->integer('product_qty')->default(0);
            $table->boolean('in_out')->default(true);
            $table->timestamps();
            $table->integer('created_by')->default(1);
            $table->integer('updated_by')->default(1);
        });

        date_default_timezone_set('Asia/Jakarta');

        DB::table('productTrans')->insert(
            [
                'trans_date' => date('Y-m-d H:i:s')
                , 'trans_no' => 'TRN001'
                , 'trans_desc' => 'This is Sample Transaction'
                , 'product_id' => 1
                , 'product_qty' => 10
                , 'created_at' => date('Y-m-d H:i:s')
                , 'updated_at' => date('Y-m-d H:i:s')
                , 'created_by' => 1
                , 'updated_by' => 1
            ]
        );
    }


    public function down()
    {
        Schema::drop('productTrans');
    }
}
