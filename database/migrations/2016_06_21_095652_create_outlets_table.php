<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateOutletsTable extends Migration
{

    public function up()
    {
        Schema::create('outlets', function (Blueprint $table) {
            $table->increments('outlet_id');
            $table->string('outlet_code', 20)->unique();
            $table->string('outlet_name');
            $table->date('outlet_active_date');
            $table->text('outlet_info');
            $table->boolean('is_active')->default(true);
            $table->index('outlet_code', 'outlet_code');
            $table->index('outlet_name', 'outlet_name');
        });

        $outlet_info = ['outlet_phone' => '234567890', 'outlet_address' => 'address 1'];

        DB::table('outlets')->insert(
            [
                'outlet_code' => 'OUT0001',
                'outlet_name' => 'sample outlet',
                'outlet_active_date' => Carbon::now()->toDateString(),
                'outlet_info' => json_encode($outlet_info)
            ]
        );

    }


    public function down()
    {
        Schema::drop('outlets');
    }
}
