<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRolesTable extends Migration
{
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('role_id');
            $table->string('role_name', 50)->unique();
            $table->boolean('system_created')->default(false);
            $table->index('system_created', 'system_created');
        });

        DB::table('roles')->insert(
            ['role_name' => 'admin'
                , 'system_created' => true
            ]);
    }

    public function down()
    {
        Schema::drop('roles');
    }
}
