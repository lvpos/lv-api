<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('product_code', 20)->unique();
            $table->string('product_name')->unique();
            $table->text('product_info');
            $table->integer('product_qty')->default(0);
            $table->decimal('product_sales_price')->default(0);
            $table->decimal('product_basic_price')->default(0);
            $table->boolean('is_active')->default(true);
        });

        $sampleInfo = [
            'description' => 'product description, it just describing a sample product',
            'image_url' => 'nopic.jpg'
        ];

        DB::table('products')->insert(
            [
                'product_code' => 'SA001'
                , 'product_name' => 'Sample Product'
                , 'product_info' => json_encode($sampleInfo)
                , 'product_qty' => 10
            ]
        );
    }

    public function down()
    {
        Schema::drop('products');
    }
}
