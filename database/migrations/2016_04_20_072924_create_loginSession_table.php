<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginSessionTable extends Migration
{

    public function up()
    {
        Schema::create('loginSession', function (Blueprint $table) {
            $table->increments('login_session_id');
            $table->string('login_session_key');
            $table->integer('user_id');
            $table->dateTime('login_time');
        });
    }


    public function down()
    {
        Schema::drop('loginSession');
    }
}
