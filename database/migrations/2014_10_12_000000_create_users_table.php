<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_email')->unique();
            $table->string('user_password');
            $table->string('user_fname', 100);
            $table->string('user_lname', 100);
            $table->integer('role_id');
            $table->boolean('is_active')->default(true);
            $table->index('role_id', 'role_id');
            $table->index('is_active', 'is_active');
        });

        DB::table('users')->insert(
            ['user_email' => 'vidi.hermes@gmail.com'
                , 'user_password' => Hash::make('password')
                , 'user_fname' => 'vidy'
                , 'user_lname' => 'hermes'
                , 'role_id' => 1
            ]
        );

    }

    public function down()
    {
        Schema::drop('users');
    }
}
