<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRoleMenusTable extends Migration
{
    public function up()
    {
        Schema::create('roleMenus', function (Blueprint $table) {
            $table->increments('role_menu_id');
            $table->integer('role_id');
            $table->integer('menu_id');
            $table->boolean('enable_access')->default(true);
            $table->boolean('enable_create')->default(true);
            $table->boolean('enable_update')->default(true);
            $table->boolean('enable_destroy')->default(true);
            $table->unique(['role_id', 'menu_id'], 'role_id_menu_id');
        });

        DB::table('roleMenus')->insert(
            ['role_id' => 1
                , 'menu_id' => 1
            ]);
        DB::table('roleMenus')->insert(
            ['role_id' => 1
                , 'menu_id' => 2
            ]);
        DB::table('roleMenus')->insert(
            ['role_id' => 1
                , 'menu_id' => 3
            ]);
        DB::table('roleMenus')->insert(
            ['role_id' => 1
                , 'menu_id' => 4
            ]);
        DB::table('roleMenus')->insert(
            ['role_id' => 1
                , 'menu_id' => 5
            ]);

        DB::table('roleMenus')->insert(
            ['role_id' => 1
                , 'menu_id' => 6
            ]);
    }


    public function down()
    {
        Schema::drop('roleMenus');
    }
}
