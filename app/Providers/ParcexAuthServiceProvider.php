<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ParcexAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('clearchar', function ($attribute, $value, $parameters, $validator) {
           return preg_replace("/[^0-9]/", '', $value);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
