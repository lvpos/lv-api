<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
/*$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api'], function () {
    Route::post('login', 'api\UserController@login')->middleware('my.auth');
    Route::post('home', 'api\HomeController@index')->middleware(['user.log']);
});

Route::group(['prefix' => 'api/user', 'middleware' => ['user.log', 'menu.exist']], function () {
    Route::post('index/{perPage?}/{keyword?}', 'api\UserController@index');
    Route::post('create', 'api\UserController@create');
    Route::post('show/{user_id?}', 'api\UserController@show');
    Route::post('update/{user_id?}', 'api\UserController@update');
    Route::post('delete/{user_id?}', 'api\UserController@destroy');
    Route::post('delete_multi', 'api\UserController@destroyMulti');
});

Route::group(['prefix' => 'api/menu', 'middleware' => ['user.log', 'menu.exist']], function () {
    Route::post('index/{page?}', 'api\MenuController@index');
    Route::post('update/{id?}', 'api\MenuController@update');
    Route::post('create', 'api\MenuController@create');
    Route::post('delete/{id?}', 'api\MenuController@destroy');
    Route::post('delete_multi', 'api\MenuController@destroyMulti');
    Route::post('get_role_template', 'api\MenuController@getRoleMenuTemplate');
});

Route::group(['prefix' => 'api/roles', 'middleware' => ['user.log', 'menu.exist']], function () {
    Route::post('index/{page?}', 'api\RoleController@index');
    Route::post('create', 'api\RoleController@create');
    Route::post('update/{id?}', 'api\RoleController@update');
    Route::post('show/{id?}', 'api\RoleController@show');
    Route::post('delete/{id?}', 'api\RoleController@destroy');
    Route::post('delete_multi', 'api\RoleController@destroyMulti');
});

Route::group(['prefix' => 'api/products'], function () {
    Route::group(['middleware' => ['user.log', 'menu.exist']], function () {
        Route::post('index/{page?}', 'api\ProductCtrlExt@index');
        Route::post('create', 'api\ProductCtrlExt@create');
        Route::post('update/{id?}', 'api\ProductCtrlExt@update');
        Route::post('show/{id?}', 'api\ProductCtrlExt@show');
        Route::post('delete/{id?}', 'api\ProductCtrlExt@destroy');
        Route::post('delete_multi', 'api\ProductCtrlExt@destroyMulti');
    });

    Route::post('active_products', 'api\ProductCtrlExt@getActiveProducts')->middleware(['user.log']);
});

Route::group(['prefix' => 'api/outlets'], function () {
    Route::group(['middleware' => ['user.log', 'menu.exist']], function () {
        Route::post('index/{page?}', 'api\OutletController@index');
        Route::post('create', 'api\OutletController@create');
        Route::post('update/{id?}', 'api\OutletController@update');
        Route::post('show/{id?}', 'api\OutletController@show');
        Route::post('delete/{id?}', 'api\OutletController@destroy');
        Route::post('delete_multi', 'api\OutletController@destroyMulti');
    });

    Route::post('active_outlets', 'api\OutletController@getActiveOutlets')->middleware(['user.log']);
});

