<?php

namespace App\Http\Controllers;

use Mail;
use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\model\haccess;


class APIController extends Controller
{

    protected $StatusCode = 200;

    public function __construct()
    {

    }

    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    public function SetStatusCode($statusCode)
    {
        $this->StatusCode = $statusCode;
        return $this;
    }

    public function RespondNotFound($message = 'Not Found')
    {
        $this->SetStatusCode(404);
        return $this->RespondWithError($message);
    }

    public function Respond($data, $headers = array())
    {
        return response()->json($data, $this->getStatusCode(), $headers, JSON_PRETTY_PRINT);
    }

    public function RespondGetDataSuccess($message = 'Success', $data = [], $type = 0)
    {
        $this->SetStatusCode(200);

        if ($type !== 0) {
            return array(
                'data' => $data,
                'success' => array(
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        return $this->Respond(array(
            'data' => $data,
            'success' => array(
                'message' => $message,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    public function RespondSuccess($message = 'Success', $type = 0)
    {
        $this->SetStatusCode(200);

        if ($type !== 0) {
            return array(
                'success' => array(
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        return $this->Respond(array(
            'success' => array(
                'message' => $message,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    public function RespondWithError($message, $type = 0)
    {
        $this->SetStatusCode(404);

        if ($type !== 0) {
            return array(
                'error' => array(
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        return $this->Respond(array(
            'error' => array(
                'message' => $message,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    public function RespondUpdateError($message, $type = 0)
    {
        $this->SetStatusCode(400);

        if ($type !== 0) {
            return array(
                'error' => array(
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        return $this->Respond(array(
            'error' => array(
                'message' => $message,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    public function RespondEmailSent($message, $type = 0)
    {
        $this->SetStatusCode(200);

        if ($type !== 0) {
            return array(
                'success' => array(
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        return $this->Respond(array(
            'success' => array(
                'message' => $message,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    public function RespondShouldLogonFirst($message = 'You Should Log On First', $type = 0)
    {
        $this->SetStatusCode(403);

        if ($type !== 0) {
            return array(
                'error' => array(
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        return $this->Respond(array(
            'error' => array(
                'message' => $message,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    public function RespondCreated($message)
    {
        $this->SetStatusCode(200);

        $this->SetStatusCode(201);
        return $this->Respond(array(
            'message' => $message
        ));
    }

    public function RespondUpdated($message, $row = 1, $type = 0)
    {
        $this->SetStatusCode(200);

        if ($type !== 0) {
            return array(
                'success' => array(
                    'message' => $message,
                    'updated_rows' => $row,
                    'status_code' => $this->getStatusCode()
                )
            );
        }

        $this->SetStatusCode(200);
        return $this->Respond(array(
            'success' => array(
                'message' => $message,
                'updated_rows' => $row,
                'status_code' => $this->getStatusCode()
            )
        ));
    }

    /*public function RespondWithPaginator($data, LengthAwarePaginator $items)
    {
        $data = array_merge(
            ['data' => $data], array(
                'paginator' => array(
                    'total_count' => $items->total(),
                    'total_pages' => ceil($items->total() / $items->perPage()),
                    'current_page' => $items->currentPage(),
                    'next_page'=>($items->currentPage()+1),
                    'limit' => $items->perPage())
            )
        );

        return $this->Respond($data);
    }*/

    public function RespondWithPaginator(array $data, $type = 0, $message = 'Success')
    {
        $this->SetStatusCode(200);

        if ($type !== 0) {
            return array_merge(
                $data,
                ['success' => [
                    'message' => $message,
                    'status_code' => $this->getStatusCode()]
                ]
            );
        }

        return $this->Respond(array_merge(
            $data,
            ['success' => [
                'message' => 'Success',
                'status_code' => $this->getStatusCode()]
            ]
        ));
    }

    public function accessLog(Request $request, $data = '')
    {
        haccess::create(['haccess_uri' => '/' . $request->path()
            , 'haccess_data' => str_replace('"', '~', $data)
            , 'created_date' => date('Y-m-d H:i:s')
            , 'app_id' => $request->header('PHP_AUTH_USER')
            , 'ip_address' => $request->ip()]);
    }

    public function sendEmail(array $body, array $data)
    {
        $format = [
            'template' => ''
            , 'sender' => 'admin@parcelexpress.sg'
            , 'sender_alias' => 'PX Admin'
            , 'receiver' => ''
            , 'subject' => 'Parcelexpress.sg | New Password'
        ];

        $format = array_merge($format, $body);
        Mail::send($format['template'], $data, function ($message) use ($format) {
            $message->from($format['sender'], $format['sender_alias']);
            $message->to($format['receiver'])->subject($format['subject']);
        });
    }

    public function messageConverter($message, array $field)
    {
        $forbid = ['[', ']', '{', '}', '"'];
        return str_replace(',', '<br>', str_replace(array_merge($forbid, $field), [''], $message));
    }

    protected function _showErrorMessage($validator, array $field)
    {
        if ($validator->fails()) {
            $message = $validator->messages()->toJson();
            $error_message = $this->messageConverter($message, $field);
            return $this->RespondWithError($error_message);
        }

        return false;
    }

}
