<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\model\outlets;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Validator;

class OutletController extends HomeController
{
    public function index(Request $request, $perPage = 25)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $outlet = outlets::with('products')->paginate($perPage)
            ->toArray();
        return $this->RespondWithPaginator($outlet);
    }

    public function create(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'outlet_data' => 'required|array'
        ]);

        $field = ['outlet_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('outlet_data'), [
            'outlet_code' => 'required|max:10|unique:outlets,outlet_code',
            'outlet_name' => 'required|max:255|unique:outlets,outlet_name',
            'outlet_active_date' => 'required|date',
            'outlet_info' => 'required|array',
            'is_active' => 'required|boolean'
        ]);

        $field = [
            'outlet_code:',
            'outlet_name:',
            'outlet_active_date:',
            'outlet_info:',
            'is_active:'
        ];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('outlet_data')['outlet_info'], [
            'outlet_phone' => 'required|between:1,50',
            'outlet_address' => 'required|max:255'
        ]);

        $field = ['outlet_phone:', 'outlet_address:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        DB::beginTransaction();
        $outletData = $request->input('outlet_data');
        $outlet_info = $outletData['outlet_info'];
        $outletData = array_merge($outletData, ['outlet_info' => json_encode($outlet_info)]);
        try {
            outlets::create($outletData);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('New Data Created');
    }

    public function update(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'outlet_data' => 'required|array'
        ]);

        $field = ['outlet_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('outlet_data'), [
            'outlet_code' => 'required|max:10|unique:outlets,outlet_code',
            'outlet_name' => 'required|max:255|unique:outlets,outlet_name',
            'outlet_active_date' => 'required|date',
            'outlet_info' => 'required|array',
            'is_active' => 'required|boolean'
        ]);

        $field = [
            'outlet_code:',
            'outlet_name:',
            'outlet_active_date:',
            'outlet_info:',
            'is_active:'
        ];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('outlet_data')['outlet_info'], [
            'outlet_phone' => 'required|between:1,50',
            'outlet_address' => 'required|max:255'
        ]);

        $field = ['outlet_phone:', 'outlet_address:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        DB::beginTransaction();
        $outletData = $request->input('outlet_data');
        $outlet_info = $outletData['outlet_info'];
        $outletData = array_merge($outletData, ['outlet_info' => json_encode($outlet_info)]);
        try {
            outlets::find($id)->update($outletData);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('Data Updated');
    }


    public function show(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $outlet = outlets::where('outlet_id', '=', $id)
            ->with('products')
            ->get()
            ->first();
        if ($outlet) {
            $outlet = $outlet->toArray();
            return $this->RespondGetDataSuccess('Success', $outlet);
        }

        return $this->RespondNotFound('Data Not Found');
    }

    public function destroy(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        try {
            $delete = outlets::destroy($id);
            if (!$delete) {
                return $this->RespondWithError('Data Has Been Use By Related Model, Please Check Data Relation');
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');
    }

    public function destroyMulti(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'outlet_ids' => 'required|array'
        ]);

        $field = ['outlet_ids:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
        $user_ids = $request->input('outlet_ids');

        try {
            $delete = outlets::destroy($user_ids);
            if (!$delete) {
                return $this->RespondWithError('Data Has Been Use By Related Model, Please Check Data Relation');
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');

    }

    public function getActiveOutlets(Request $request)
    {
        $outlets = outlets::where('is_active', '=', true)
            ->get(['outlet_id', 'outlet_code', 'outlet_name']);
        if ($outlets) {
            $outlets = $outlets->toArray();
        } else {
            $outlets = [];
        }

        return $this->RespondGetDataSuccess('Success', $outlets);
    }
}
