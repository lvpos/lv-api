<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\model\roleMenus;
use App\model\roles;
use App\model\shoes;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Validator;
use Illuminate\Support\Facades\File;
use DB;

class RoleController extends HomeController
{

    public function index(Request $request, $perPage = 25)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $users = roles::with(['roleMenus' => function ($query) {
            $query->select('roleMenus.*', 'menus.menu_name')
                ->leftJoin('menus', 'roleMenus.menu_id', '=', 'menus.menu_id');
        }])->paginate($perPage)->toArray();

        return $this->RespondWithPaginator($users);
    }

    public function create(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'role_data' => 'required|array',
            'role_menu_data' => 'required|array'
        ]);

        $field = ['role_data:', 'role_menu_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('role_data'), [
            'role_name' => 'required|max:50|unique:roles,role_name'
        ]);

        $field = ['role_name:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $role_data = $request->input('role_data');
        $role_menu_data = $request->input('role_menu_data');

        foreach ($role_menu_data as $rm) {
            $validator = Validator::make($rm, [
                'menu_id' => 'required|integer',
                'enable_access' => 'required|boolean',
                'enable_create' => 'required|boolean',
                'enable_update' => 'required|boolean',
                'enable_destroy' => 'required|boolean'
            ]);

            $field = ['menu_id:',
                'enable_access:',
                'enable_create:',
                'enable_update:',
                'enable_destroy:'];
            $error = $this->_showErrorMessage($validator, $field);
            if ($error) {
                return $error;
            }
        }

        $res = [];
        try {

            $new_role = roles::create($role_data);

            foreach ($role_menu_data as $rm) {
                array_push($res, array_merge($rm, ['role_id' => $new_role->role_id]));
            }

            roleMenus::insert($res);
            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('New Data Created');
    }

    public function update(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'role_data' => 'required|array',
            'role_menu_data' => 'required|array'
        ]);

        $field = ['role_data:', 'role_menu_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('role_data'), [
            'role_name' => 'required|max:50|unique:roles,role_name,' . $id . ',role_id'
        ]);

        $field = ['role_name:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $role_data = $request->input('role_data');
        $role_menu_data = $request->input('role_menu_data');

        foreach ($role_menu_data as $rm) {
            $validator = Validator::make($rm, [
                'menu_id' => 'required|integer',
                'enable_access' => 'required|boolean',
                'enable_create' => 'required|boolean',
                'enable_update' => 'required|boolean',
                'enable_destroy' => 'required|boolean'
            ]);

            $field = ['menu_id:',
                'enable_access:',
                'enable_create:',
                'enable_update:',
                'enable_destroy:'];
            $error = $this->_showErrorMessage($validator, $field);
            if ($error) {
                return $error;
            }
        }

        $res = [];
        try {
            $new_role = roles::find($id);
            $new_role->roleMenus()->delete();
            foreach ($role_menu_data as $rm) {
                array_push($res, array_merge($rm, ['role_id' => $new_role->role_id]));
            }
            roleMenus::insert($res);
            $new_role->update($role_data);
            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Updated');
    }

    public function show(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $role = roles::where('role_id', '=', $id)
            ->with(
                ['roleMenus' => function ($query) {
                    $query->select('roleMenus.*', 'menus.menu_name')
                        ->leftJoin('menus', 'roleMenus.menu_id', '=', 'menus.menu_id');
                }]
            )->get()
            ->first();
        if ($role) {
            $role = $role->toArray();
            return $this->RespondGetDataSuccess('Success', $role);
        }

        return $this->RespondNotFound('Data Not Found');
    }

    public function destroy(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $system_created = roles::where('role_id', '=', $id)
            ->where('system_created', '=', true)
            ->get()
            ->first();
        if ($system_created) {
            return $this->RespondWithError('System Created Role Can Not Be Deleted');
        }

        try {
            $role = roles::find($id);
            $role->roleMenus()->delete();
            roles::destroy($id);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');
    }

    public function destroyMulti(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'role_ids' => 'required|array'
        ]);

        $field = ['role_ids:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
        $role_ids = $request->input('role_ids');

        $system_created = roles::whereIn('role_id', $role_ids)
            ->where('system_created', '=', true)
            ->get()
            ->toArray();

        if ($system_created) {
            return $this->RespondWithError('System Created Role Can Not Be Deleted');
        }

        try {
            roles::destroy($role_ids);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');

    }

}
