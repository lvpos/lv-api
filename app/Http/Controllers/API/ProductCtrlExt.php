<?php

namespace App\Http\Controllers\API;

use App\model\products;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\File;
use Validator;
use Illuminate\Support\Facades\URL;

class ProductCtrlExt extends ProductController
{


    public function create(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'product_data' => 'required|array'
        ]);

        $field = ['product_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('product_data'), [
            'product_code' => 'required|max:10|unique:products,product_code',
            'product_name' => 'required|max:255|unique:products,product_name',
            'product_sales_price' => 'required|numeric|digits_between:1,20',
            'product_basic_price' => 'required|numeric|digits_between:1,20',
            'product_info' => 'required|array',
            'is_active' => 'required|boolean'
        ]);

        $field = [
            'product_code:',
            'product_name:',
            'product_sales_price:',
            'product_basic_price:',
            'product_info:',
            'is_active:'
        ];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->all()['product_data']['product_info'], [
            'description' => 'required|max:255',
            'files' => 'image|max:50'
        ]);


        $field = ['description:', 'files:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $extension = '';
        $filename = 'nopic.jpg';
        $path = '';
        $file = $request->all()['product_data']['product_info']['files'];
        if ($file) {
            $extension = $file->getClientOriginalExtension();
            $now = Carbon::now();
            $filename = str_replace(":", "", str_replace(" ", "", str_replace("-", "", $now->toDateTimeString()))) . '.' . $extension;
            $path = storage_path() . '/images';
        }


        DB::beginTransaction();
        $productData = $request->input('product_data');
        $product_info = $productData['product_info'];
        array_push($product_info, ['image_url' => $filename]);
        $productData = array_merge($productData, ['product_info' => json_encode($product_info)]);

        try {
            products::create($productData);
            DB::commit();
            if ($file) {
                $file->move($path, $filename);
            }
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('New Data Created');
    }

    public function update(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'product_data' => 'required|array'
        ]);

        $field = ['product_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('product_data'), [
            'product_code' => 'required|max:10|unique:products,product_code,' . $id . ',product_id',
            'product_name' => 'required|max:255|unique:products,product_name,' . $id . ',product_id',
            'product_sales_price' => 'required|numeric|digits_between:1,20',
            'product_basic_price' => 'required|numeric|digits_between:1,20',
            'product_info' => 'required|array',
            'is_active' => 'required|boolean'
        ]);

        $field = [
            'product_code:',
            'product_name:',
            'product_sales_price:',
            'product_basic_price:',
            'product_info:',
            'is_active:'
        ];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->all()['product_data']['product_info'], [
            'description' => 'required|max:255',
            'files' => 'image|max:50'
        ]);


        $field = ['description:', 'files:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $extension = '';
        $filename = 'nopic.jpg';
        $path = '';
        $file = $request->all()['product_data']['product_info']['files'];
        if ($file) {
            $extension = $file->getClientOriginalExtension();
            $now = Carbon::now();
            $filename = str_replace(":", "", str_replace(" ", "", str_replace("-", "", $now->toDateTimeString()))) . '.' . $extension;
            $path = storage_path() . '/images';
        }

        DB::beginTransaction();
        $productData = $request->input('product_data');
        $product_info = $productData['product_info'];
        if ($file) {
            $product_info = array_merge($product_info, ['image_url' => $filename]);
        }
        $productData = array_merge($productData, ['product_info' => json_encode($product_info)]);

        try {
            products::find($id)->update($productData);
            DB::commit();
            if ($file) {
                $file->move($path, $filename);
            }
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('Data Updated');
    }


}
