<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Validator;
use App\model\users;
use App\model\loginSession;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends HomeController
{

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $field = ['email:'
            , 'password:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $email = $request->input('email');
        $password = $request->input('password');
        $user = $this->_checkValidUser($email, $password);

        if ($user) {
            $session_key = $this->_createSession($user->user_id);
            return $this->RespondGetDataSuccess('You Have Successfully Logged In', array_merge($user->toArray(), ['session_key' => $session_key]));
        }
        return $this->RespondWithError('Login Failed, Username & Password Combination Not Match');
    }

    private function _checkValidUser($email = '', $password = '')
    {
        $user = users::where('user_email', '=', $email)
            ->get()
            ->first();
        if (Hash::check($password, $user->user_password)) {
            return $user;
        }
    }

    private function _createSession($user_id = 0)
    {
        date_default_timezone_set('Asia/Jakarta');

        loginSession::where('user_id', '=', $user_id)
            ->delete();

        $session_key = str_random(60);
        loginSession::create(
            [
                'login_session_key' => $session_key,
                'user_id' => $user_id,
                'login_time' => date('Y-m-d H:i:s')
            ]);

        return $session_key;
    }

    public function index(Request $request, $perPage = 50, $keyword = '')
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $users = users::leftJoin('roles', 'users.role_id', '=', 'roles.role_id')
            ->select('users.*', 'roles.role_name');
        if ($keyword != '') {
            $users = $users->whereRaw('LCASE(CONCAT(users.user_fname,users.user_lname,users.user_email,roles.role_name)) LIKE "%' . $keyword . '%"');
        }
        $users = $users->paginate($perPage)->toArray();
        return $this->RespondWithPaginator($users);
    }

    public function create(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'user_data' => 'required|array'
        ]);

        $field = ['user_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('user_data'), [
            'user_email' => 'required|email|unique:users,user_email',
            'user_password' => 'required',
            'user_fname' => 'required|max:100',
            'user_lname' => 'required|max:100',
            'role_id' => 'required|integer'
        ]);

        $field = ['user_email:'
            , 'user_password:'
            , 'user_fname:'
            , 'user_lname:'
            , 'role_id:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $new_data = $request->input('user_data');
        $new_data = array_merge($new_data, ['user_password' => Hash::make($new_data['user_password'])]);
        try {
            users::create($new_data);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('New User Data Created');

    }

    public function show(Request $request, $user_id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $user = users::find($user_id);
        if ($user) {
            $user = $user->toArray();
            return $this->RespondGetDataSuccess('Success', $user);
        }

        return $this->RespondNotFound('Data Not Found');
    }

    public function update(Request $request, $user_id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_update']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'user_data' => 'required|array'
        ]);

        $field = ['user_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('user_data'), [
            'user_email' => 'required|email|unique:users,user_email,' . $user_id . ',user_id',
            'user_fname' => 'required|max:100',
            'user_lname' => 'required|max:100',
            'role_id' => 'required|integer'
        ]);

        $field = ['user_email:'
            , 'user_fname:'
            , 'user_lname:'
            , 'role_id:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $new_data = $request->input('user_data');
        if (array_key_exists('user_password', $new_data)) {
            $new_data = array_merge($new_data, ['user_password' => Hash::make($new_data['user_password'])]);
        }

        try {
            users::where('user_id', '=', $user_id)
                ->update($new_data);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Updated');
    }

    public function destroy(Request $request, $user_id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        try {
            users::destroy($user_id);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        $users = users::leftJoin('roles', 'users.role_id', '=', 'roles.role_id')
            ->select('users.*', 'roles.role_name');
        $users = $users->paginate(50)->toArray();
        return $this->RespondWithPaginator($users, 0, 'Data Deleted');
    }

    public function destroyMulti(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'user_ids' => 'required|array'
        ]);

        $field = ['user_ids:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
        $user_ids = $request->input('user_ids');

        try {
            users::destroy($user_ids);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        $users = users::leftJoin('roles', 'users.role_id', '=', 'roles.role_id')
            ->select('users.*', 'roles.role_name');
        $users = $users->paginate(50)->toArray();
        return $this->RespondWithPaginator($users, 0, 'Data Deleted');

    }

}
