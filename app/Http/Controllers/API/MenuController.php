<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use App\model\menus;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Validator;
use Illuminate\Support\Facades\File;
use DB;

class MenuController extends HomeController
{

    public function index(Request $request, $perPage = 25)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $users = menus::paginate($perPage)->toArray();
        return $this->RespondWithPaginator($users);
    }

    public function create(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'menu_data' => 'required|array'
        ]);

        $field = ['menu_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('menu_data'), [
            'menu_name' => 'required|max:50',
            'menu_parent_id' => 'required|integer',
            'menu_priority' => 'required|integer',
            'is_active' => 'required|boolean',
            'is_parent' => 'required|boolean',
            'menu_url' => 'required|max:255'
        ]);

        $field = ['menu_name:'
            , 'menu_parent_id:'
            , 'menu_priority:'
            , 'is_active:'
            , 'is_parent:'
            , 'menu_url:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $menu_data = $request->input('menu_data');

        $data = [
            'menu_name' => $menu_data['menu_name']
            , 'menu_parent_id' => $menu_data['menu_parent_id']
            , 'menu_priority' => $menu_data['menu_priority']
            , 'is_active' => $menu_data['is_active']
            , 'is_parent' => $menu_data['is_parent']
            , 'menu_url' => $menu_data['menu_url']
        ];

        try {
            menus::create($data);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('New Data Created');
    }

    public function update(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_update']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'menu_data' => 'required|array'
        ]);

        $field = ['menu_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('menu_data'), [
            'menu_name' => 'required|max:50',
            'menu_parent_id' => 'required|integer',
            'menu_priority' => 'required|integer',
            'is_active' => 'required|boolean',
            'is_parent' => 'required|boolean'
        ]);

        $field = ['menu_name:'
            , 'menu_parent_id:'
            , 'menu_priority:'
            , 'is_active:'
            , 'is_parent:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $menu_data = $request->input('menu_data');

        $data = [
            'menu_name' => $menu_data['menu_name']
            , 'menu_parent_id' => $menu_data['menu_parent_id']
            , 'menu_priority' => $menu_data['menu_priority']
            , 'is_active' => $menu_data['is_active']
            , 'is_parent' => $menu_data['is_parent']
        ];

        try {
            menus::where('menu_id', '=', $id)
                ->update($data);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Updated');
    }

    public function destroy(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $system_created = menus::where('menu_id', '=', $id)
            ->where('system_created', '=', true)
            ->get()
            ->first();
        if ($system_created) {
            return $this->RespondWithError('System Created Menu Can Not Be Deleted');
        }

        try {
            menus::destroy($id);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');

    }

    public function destroyMulti(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'menu_ids' => 'required|array'
        ]);

        $field = ['menu_ids:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
        $menu_ids = $request->input('menu_ids');

        $system_created = menus::whereIn('menu_id', $menu_ids)
            ->where('system_created', '=', true)
            ->get()
            ->toArray();

        if ($system_created) {
            return $this->RespondWithError('System Created Menu Can Not Be Deleted');
        }

        try {
            menus::destroy($menu_ids);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');

    }

    public function getRoleMenuTemplate(Request $request)
    {
        $menu_list = menus::select(DB::raw('menu_id,menu_name,0 as enable_access,0 as enable_update,0 as enable_destroy'))
            ->get()
            ->toArray();
        return $this->RespondGetDataSuccess('Success', $menu_list);
    }

}
