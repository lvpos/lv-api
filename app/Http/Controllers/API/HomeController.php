<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\File;

use App\model\loginSession;
use App\model\roles;
use App\model\users;
use App\model\roleMenus;


class HomeController extends APIController
{

    public function index(Request $request)
    {
        return $this->_showMenus($request);
    }

    protected function _showMenus(Request $request)
    {
        $user_id = $request->input('user_id');
        $user_role_id = $request->input('user_role_id');
        $session_key = $request->input('session_key');
        $roles = roles::where('role_id', '=', $user_role_id)
            ->with(['menus' => function ($query) {
                $query->where('is_active', '=', true);
                $query->where('is_parent', '=', true);
                $query->where('menu_parent_id', '=', 0);
                $query->with('childMenu');
            }])
            ->get()
            ->first()
            ->toArray();

        $user = users::find($user_id)->toArray();

        $result = array_merge($roles, $user, ['session_key' => $session_key]);
        return $this->RespondGetDataSuccess('Success', $result);
    }

    protected function _isActionEnable($menu_id = 0, $user_role_id = 0, $action = ['enable_access'])
    {
        $allow = roleMenus::where('menu_id', '=', $menu_id)
            ->where('role_id', '=', $user_role_id)
            ->get($action)
            ->first();

        if ($allow) {
            $enable = array_values($allow->toArray())[0];

            if (!$enable) {
                return $this->RespondWithError('Your Role Are Not Permitted to Perform This Action');
            }
        } else {
            return $this->RespondWithError('Your Role Are Not Permitted to Perform This Action');
        }

    }

}
