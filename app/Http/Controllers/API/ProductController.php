<?php

namespace App\Http\Controllers\API;

use App\model\products;
use App\model\shoes;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Validator;

class ProductController extends HomeController
{

    public function index(Request $request, $perPage = 25)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $product = shoes::paginate($perPage)
            ->toArray();
        return $this->RespondWithPaginator($product);
    }

    public function create(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'product_data' => 'required|array'
        ]);

        $field = ['product_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('product_data'), [
            'product_code' => 'required|max:10|unique:products,product_code',
            'product_name' => 'required|max:255|unique:products,product_name',
            'product_sales_price' => 'required|numeric|digits_between:1,20',
            'product_basic_price' => 'required|numeric|digits_between:1,20',
            'product_info' => 'required|array',
            'is_active' => 'required|boolean'
        ]);

        $field = [
            'product_code:',
            'product_name:',
            'product_sales_price:',
            'product_basic_price:',
            'product_info:',
            'is_active:'
        ];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('product_data')['product_info'], [
            'size' => 'required|max:255',
            'description' => 'required|max:255'
        ]);

        $field = ['size:', 'description:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        DB::beginTransaction();
        $productData = $request->input('product_data');
        $product_info = $productData['product_info'];
        $productData = array_merge($productData, ['product_info' => json_encode($product_info)]);
        try {
            products::create($productData);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('New Data Created');
    }

    public function update(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_create']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'product_data' => 'required|array'
        ]);

        $field = ['product_data:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('product_data'), [
            'product_code' => 'required|max:10|unique:products,product_code,' . $id . ',product_id',
            'product_name' => 'required|max:255|unique:products,product_name,' . $id . ',product_id',
            'product_sales_price' => 'required|numeric|digits_between:1,20',
            'product_basic_price' => 'required|numeric|digits_between:1,20',
            'product_info' => 'required|array',
            'is_active' => 'required|boolean'
        ]);

        $field = [
            'product_code:',
            'product_name:',
            'product_sales_price:',
            'product_basic_price:',
            'product_info:',
            'is_active:'
        ];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        $validator = Validator::make($request->input('product_data')['product_info'], [
            'size' => 'required|max:255',
            'description' => 'required|max:255'
        ]);

        $field = ['size:', 'description:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }

        DB::beginTransaction();
        $productData = $request->input('product_data');
        $product_info = $productData['product_info'];
        $productData = array_merge($productData, ['product_info' => json_encode($product_info)]);
        try {
            products::find($id)->update($productData);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }

        return $this->RespondSuccess('Data Updated');
    }


    public function show(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_access']);
        if ($allow) {
            return $allow;
        }

        $product = products::find($id);
        if ($product) {
            $product = $product->toArray();
            return $this->RespondGetDataSuccess('Success', $product);
        }

        return $this->RespondNotFound('Data Not Found');
    }

    public function destroy(Request $request, $id = 0)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        try {
            $delete = products::destroy($id);
            if (!$delete) {
                return $this->RespondWithError('Data Has Been Use By Related Model, Please Check Data Relation');
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');
    }

    public function destroyMulti(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $user_role_id = $request->input('user_role_id');
        $allow = $this->_isActionEnable($menu_id, $user_role_id, ['enable_destroy']);
        if ($allow) {
            return $allow;
        }

        $validator = Validator::make($request->all(), [
            'product_ids' => 'required|array'
        ]);

        $field = ['product_ids:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
        $user_ids = $request->input('product_ids');

        try {
            $delete = products::destroy($user_ids);
            if (!$delete) {
                return $this->RespondWithError('Data Has Been Use By Related Model, Please Check Data Relation');
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            $message = $e->errorInfo[2];
            return $this->RespondWithError($message);
        }
        return $this->RespondSuccess('Data Deleted');

    }

    public function getActiveProducts(Request $request)
    {
        $products = products::where('is_active', '=', true)
            ->get(['product_id', 'product_code', 'product_name']);
        if ($products) {
            $products = $products->toArray();
        } else {
            $products = [];
        }

        return $this->RespondGetDataSuccess('Success', $products);
    }
}
