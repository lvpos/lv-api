<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\model\users;
use App\model\loginSession;
use Validator;

class userLogAuth extends borobudurBaseAuth
{
    public function handle($request, Closure $next)
    {
        $failed = $this->_paramValidation($request);
        if ($failed) {
            return $failed;
        }

        $notLogged = $this->_loginCheck($request);
        if ($notLogged) {
            return $notLogged;
        }

        $notUser = $this->_validateUserRole($request);
        if ($notUser) {
            return $notUser;
        }

        return $next($request);
    }

    private function _paramValidation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'user_role_id' => 'required|integer',
            'menu_id' => 'required|integer',
            'session_key' => 'required|size:60'
        ]);

        $field = ['user_id:'
            , 'user_role_id:'
            , 'menu_id:'
            , 'session_key:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
    }


    protected function _loginCheck(Request $request)
    {
        $user_id = $request->input('user_id');
        $session_key = $request->input('session_key');

        $login_session = loginSession::where('user_id', '=', $user_id)
            ->where('login_session_key', '=', $session_key)
            ->get()
            ->first();

        if (!$login_session) {
            return $this->_respondWithError('Please Login In Order to Access');
        }
    }

    protected function _validateUserRole(Request $request)
    {
        $user_id = $request->input('user_id');
        $user_role_id = $request->input('user_role_id');

        $user = users::where('user_id', '=', $user_id)
            ->where('role_id', '=', $user_role_id)
            ->get()
            ->first();
        if (!$user) {
            return $this->_respondWithError('Combination of User and Role Not Match');
        }
    }
}
