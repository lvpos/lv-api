<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\model\menus;
use Validator;

class menuExistAuth extends borobudurBaseAuth
{

    public function handle($request, Closure $next)
    {
        $failed = $this->_paramValidation($request);
        if ($failed) {
            return $failed;
        }

        $notExist = $this->_menuCheck($request);
        if ($notExist) {
            return $notExist;
        }

        return $next($request);
    }

    private function _paramValidation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'menu_id' => 'required|integer'
        ]);

        $field = ['menu_id:'];
        $error = $this->_showErrorMessage($validator, $field);
        if ($error) {
            return $error;
        }
    }

    protected function _menuCheck(Request $request)
    {
        $menu_id = $request->input('menu_id');
        $menu = menus::find($menu_id);
        if (!$menu) {
            return $this->_respondWithError('Menu You Are Trying To Access Does Not Exist');
        }
    }
}
