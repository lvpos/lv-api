<?php

namespace App\Http\Middleware;

use Closure;

class borobudurBaseAuth
{
    protected $status_code = 200;

    public function handle($request, Closure $next)
    {
        return $next($request);

    }

    protected function _showErrorMessage($validator, array $field)
    {
        if ($validator->fails()) {
            $message = $validator->messages()->toJson();
            $error_message = $this->_messageConverter($message, $field);
            return $this->_respondWithError($error_message);
        }

        return false;
    }

    protected function _messageConverter($message, array $field)
    {
        $forbid = ['[', ']', '{', '}', '"'];
        return str_replace(',', '<br>', str_replace(array_merge($forbid, $field), [''], $message));
    }


    protected function _respondWithError($message)
    {
        $this->_setStatusCode(404);

        return $this->_respond(array(
            'error' => array(
                'message' => $message,
                'status_code' => $this->_getStatusCode()
            )
        ));
    }

    protected function _respond($data, $headers = array())
    {
        return response()->json($data, $this->_getStatusCode(), $headers, JSON_PRETTY_PRINT);
    }

    protected function _getStatusCode()
    {
        return $this->status_code;
    }

    protected function _setStatusCode($statusCode)
    {
        $this->status_code = $statusCode;
        return $this;
    }
}
