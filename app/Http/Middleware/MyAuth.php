<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class MyAuth
{
    public function handle($request, Closure $next)
    {

        if (!$this->basic_validate($request->header('PHP_AUTH_USER'), $request->header('PHP_AUTH_PW'))) {
            $data = ['error' => ['message' => 'Failed to Pass HTTP Authentication', 'status_code' => 404]];
            return response()->json($data, 404, array(), JSON_PRETTY_PRINT);
        }
        return $next($request);
    }

    private function basic_validate($user, $password)
    {
        if ($user == 'vidy' && $password == 'pvidy') {

            return true;
        }

        return false;
    }


}
