<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    public $timestamps = false;
    protected $guarded = ['user_id'];
    protected $hidden = ['user_password'];

    public function role()
    {
        return $this->belongsTo('App\model\roles', 'role_id');
    }

}
