<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class outletProducts extends Model
{
    protected $table = 'outletProducts';
    protected $primaryKey = 'outlet_product_id';
    public $timestamps = true;
    protected $guarded = ['outlet_product_id'];

    public function product()
    {
        return $this->belongsTo('App\model\products', 'product_id');
    }

    public function outlet()
    {
        return $this->belongsTo('App\model\outlets', 'outlet_id');
    }

}
