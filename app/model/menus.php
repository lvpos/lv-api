<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class menus extends Model
{
    protected $table = 'menus';
    protected $primaryKey = 'menu_id';
    public $timestamps = false;
    protected $guarded = ['menu_id'];

    public function childMenu()
    {
        return $this->hasMany('App\model\menus', 'menu_parent_id', 'menu_id')->where('is_active', '=', true);
    }

}
