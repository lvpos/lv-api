<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class roleMenus extends Model
{
    protected $table = 'roleMenus';
    protected $primaryKey = 'role_menu_id';
    public $timestamps = false;
    protected $guarded = ['role_menu_id'];
    protected $hidden = ['role_menu_id'];

}
