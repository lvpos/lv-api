<?php

namespace App\model;

use App\model\products;
use Illuminate\Database\Eloquent\Model;

class shoes extends products
{
    protected $infoTemplate = [
        'size' => 's@@@'
        , 'description' => 'd@@@'
        , 'color' => 'c@@@'
    ];

}
