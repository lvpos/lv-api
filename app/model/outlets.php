<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class outlets extends Model
{
    protected $table = 'outlets';
    protected $primaryKey = 'outlet_id';
    public $timestamps = false;
    protected $guarded = ['outlet_id'];

    public function products()
    {
        return $this->hasManyThrough('App\model\products', 'App\model\outletProducts', 'outlet_id', 'product_id');
    }

    protected static function boot()
    {
        static::deleting(function ($outlet) {

            if ($outlet->products()->first()) {
                return false;
            }
        });
    }

    public function getOutletInfoAttribute($value)
    {
        return json_decode($value);
    }
}
