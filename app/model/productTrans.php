<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class productTrans extends Model
{
    protected $table = 'productTrans';
    protected $primaryKey = 'trans_id';
    public $timestamps = true;
    protected $guarded = ['trans_id'];

    public function product()
    {
        return $this->belongsTo('App\model\products', 'product_id');
    }

    public function createdUser()
    {
        return $this->hasOne('App\model\users', 'user_id', 'created_by');
    }

    public function updatedUser()
    {
        return $this->hasOne('App\model\users', 'user_id', 'updated_by');
    }
}
