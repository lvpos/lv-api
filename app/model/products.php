<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    protected $infoTemplate = [
        'size' => 's@@@'
        , 'description' => 'd@@@'
    ];

    protected $table = 'products';
    protected $primaryKey = 'product_id';
    public $timestamps = false;
    protected $guarded = ['product_id'];

    public function productTrans()
    {
        return $this->hasMany('App\model\productTrans', 'product_id', 'product_id');
    }

    public function outletProducts()
    {
        return $this->hasMany('App\model\outletProducts', 'product_id', 'product_id');
    }

    protected static function boot()
    {
        static::deleting(function ($product) {

            if ($product->productTrans()->first()) {
                return false;
            }

            if ($product->outletProducts()->first()) {
                return false;
            }
        });
    }

    public function getProductInfoAttribute($value)
    {
        return json_decode($value);
    }

    public function getInfoTemplate()
    {
        return $this->infoTemplate;
    }

    public function setInfoTemplate($template = [])
    {
        $this->infoTemplate = $template;
        return $this->getInfoTemplate();
    }

}
