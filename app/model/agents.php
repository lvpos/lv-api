<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class agents extends Model
{
    protected $table = 'tb_agents';
    protected $primaryKey = 'agent_id';
    public $timestamps = false;
    protected $guarded = ['agent_id', 'agent_no'];
    protected $hidden = ['agent_pass'];

    public function orders()
    {
        return $this->hasMany('App\model\orders', 'agent_id');
    }

    public function getAgentCnoAttribute($value)
    {
        return json_decode(str_replace('~', '"', $value));
    }

    public function trucks()
    {
        return $this->hasMany('App\model\trucks', 'truck_user');
    }

    public function childAgent()
    {
        return $this->hasManyThrough('App\model\agents', 'App\model\driver_agents', 'agent_id', 'agent_id')->where('agent_wstatus', '=', 1)->where('is_active', '=', 1);
    }
}
