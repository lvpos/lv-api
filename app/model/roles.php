<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;


class roles extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'role_id';
    public $timestamps = false;
    protected $guarded = ['role_id'];


    public function users()
    {
        return $this->hasMany('App\model\users', 'role_id');
    }

    public function roleMenus()
    {
        return $this->hasMany('App\model\roleMenus', 'role_id', 'role_id');
    }

    public function menus()
    {
        return $this->hasManyThrough('App\model\menus', 'App\model\roleMenus', 'role_id', 'menu_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($role) {
            $role->roleMenus()->delete();
        });
    }

}
