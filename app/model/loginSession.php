<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class loginSession extends Model
{
    protected $table = 'loginSession';
    protected $primaryKey = 'login_session_id';
    public $timestamps = false;
    protected $guarded = ['login_session_id'];
}
